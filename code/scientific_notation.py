def mantissa_exponent(value, base):
    n = 0
    while value >= base:
        value //= base
        n += 1
    return value, n

def mantissa_exponent_base2(value, power):
    value = int(value)
    mask = ~((2 << power-1) - 1)
    n = 0
    while value & mask:
        value >>= power
        n += 1
    return value, n

def _gen_suffixes(transform=lambda s: s):
    base_suffixes = 'KMGT'
    suffixes = [''] + list(map(transform, base_suffixes))
    return dict(enumerate(suffixes))

MetricSuffixes = _gen_suffixes()
BinarySuffixes = _gen_suffixes(lambda s: s+'i')

def format_metric(value):
    m, e = mantissa_exponent(value, 1000)
    return '%d%s' % (m, MetricSuffixes[e])

def format_binary(value, add_i=False):
    m, e = mantissa_exponent_base2(value, 10)
    suffix = MetricSuffixes[e]
    suffix = suffix + 'i' if add_i and suffix else suffix
    return '%d%s' % (m, suffix)
