HICPRO_VER=11a5e6dfb6ebb6df8de073ff556c8415fe83c7e5
HICPRO_VER_NAME=2.11.0-beta
HICPRO_URL=https://github.com/nservant/HiC-Pro/archive/${HICPRO_VER}.zip
CONDA_URL=https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh
HICPRO_ENV_ARGS=python=2.7 pip samtools pysam bx-python numpy scipy

CONDA=deps/conda2
HICPRO=deps/HiC-Pro_${HICPRO_VER_NAME}
HICPRO_ENV=${CONDA}/envs/hicpro

define HICPRO_INSTALL_CONFIG
PREFIX=${CURDIR}/deps
BOWTIE2_PATH=/software/UHTS/Aligner/bowtie2/2.3.1/bin/
SAMTOOLS_PATH=${HICPRO_ENV}
R_PATH=/software/R/3.4.2/bin/
PYTHON_PATH=${HICPRO_ENV}
CLUSTER_SYS=LSF
endef
export HICPRO_INSTALL_CONFIG

.PHONY: conda hic_env hicpro data touch_pkgs

conda: ${CONDA}
hicpro_env: ${HICPRO_ENV}
hicpro: ${HICPRO}

${CONDA}:
	$(eval installer=$(shell mktemp))
	wget ${CONDA_URL} -O ${installer}
	bash ${installer} -b -p $@
	cp cfg/condarc ${CONDA}/.condarc
	rm ${installer}
	find $@ | xargs touch -a -m
	rm '(dev).tmpl'
	rm 'manifest.xml'

${HICPRO_ENV}: ${CONDA}
	${CONDA}/bin/conda create -n hicpro -y ${HICPRO_ENV_ARGS}
	find $@ ! -newer $@ | xargs touch -a -m
	rm '(dev).tmpl'
	rm 'manifest.xml'

${HICPRO}: ${HICPRO_ENV}
	mkdir -p deps/hicpro
	$(eval TMP=$(shell mktemp -d))
	echo ${TMP}
	wget ${HICPRO_URL} -O ${TMP}/source.zip
	cd ${TMP} && unzip source.zip
	$(eval SRC=$(shell echo ${TMP}/HiC-Pro-${HICPRO_VER}))
	echo "$$HICPRO_INSTALL_CONFIG" >${SRC}/config-install.txt
	source ${CONDA}/bin/activate hicpro && cd ${SRC} && make configure && make install
	rm -rf ${TMP}
	find $@ | xargs touch -a -m

fragments.bed: ../references/hg19/hg19.fa ${HICPRO_ENV}
	${HICPRO_ENV}/bin/python ${HICPRO}/bin/utils/digest_genome.py -r mboI -o $@ $<

gen/hicpro-run-unrel: cfg/hicpro-run.templ
	mkdir -p gen
	echo '# GENERATED from hicpro-run.templ' >$@
	sed -e s/REF_SUFFIX/unrel/ -e /ALLELE_SPECIFIC_SNP/d <$< >>$@

gen/hicpro-run-twins: cfg/hicpro-run.templ
	mkdir -p gen
	echo '# GENERATED from hicpro-run.templ' >$@
	sed -e s/REF_SUFFIX/twins/ <$< >>$@

.PHONY: current_commit
current_commit:
	#git diff-index --quiet HEAD --
	#git push
	$(eval COMMIT := $(shell git rev-parse --short HEAD))
	echo -n ${COMMIT} >gen/current_commit

SAMPLES=$(abspath ../samples)
# runs/{production,truncated}/{twins,unrel}
runs/%: gen/hicpro-run-twins gen/hicpro-run-unrel current_commit
	$(eval PARTS := $(subst /, ,$@))
	$(eval RUNTYPE := $(word 2, ${PARTS}))
	$(eval GROUP := $(word 3, ${PARTS}))
	$(eval CFG := gen/hicpro-run-${GROUP})
	$(eval IN := ${SAMPLES}/${GROUP}/${RUNTYPE}/combined)
	git diff-index --quiet HEAD --
	git push
	$(eval COMMIT := $(shell cat gen/current_commit))
	$(eval OUT := $@/${COMMIT})
	${HICPRO}/bin/HiC-Pro -i ${IN} -c ${CFG} -o ${OUT} -p

	$(eval RUNINFO := ${OUT}/run-info)
	echo 'Ilya Kolpakov (ilya.kolpakov@gmail.com)' >${RUNINFO}
	date >>${RUNINFO}
	pwd >>${RUNINFO}
	git remote get-url origin >>${RUNINFO}
	git rev-parse HEAD >>${RUNINFO}

.PHONY: runs
runs: runs/production/twins runs/production/unrel runs/truncated/twins runs/truncated/unrel
